package form;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegisterServ
 */
@WebServlet("/RegisterServ")
public final class RegisterServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServ() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		
		PrintWriter out;
		try {
			out = response.getWriter();
			boolean accepted = Boolean.parseBoolean(request.getParameter("agreement"));
			if (accepted == false) {
				out.println("Cannot register without first accepting the TnC!");
			} else {
				String name = request.getParameter("name");
				String pwrd = request.getParameter("pwrd");
				String email = request.getParameter("email");
				String gender = request.getParameter("gender");
				String course = request.getParameter("course");
				if (name.isEmpty() || pwrd.isEmpty() || email.isEmpty()) {
					out.println("One or more fields empty\nRegistration failed");
				} else {
					out.println("Name: " + name);
					out.println("Password: " + "*".repeat(pwrd.length()));
					out.println("E-mail Address: " + email);
					out.println("Gender: " + gender);
					out.println("Course: " + course);
					out.println("\nREGISTRATION COMPLETE");
					
				}
				
				
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
